import sys

def skipLines(f, numLines=0):
    for i in range(numLines):
        f.next()

def getGate(flightNum, flights, numFlights, schedule, numUsedGates,
            visited=set()):
    if not schedule[flightNum] is None:
        return schedule[flightNum];

    if flights[flightNum] >= numFlights or flightNum in visited:
        schedule[flightNum] = numUsedGates
        return schedule[flightNum]

    visited.add(flightNum)
    schedule[flightNum] = getGate(flights[flightNum], flights, numFlights,
                                  schedule, numUsedGates, visited)
    return schedule[flightNum]

with open(sys.argv[1], 'r') as f:
    skipLines(f, 2)

    vars = [line.split()[0][1:].split('_') for line in f]
    numFlights = int(sys.argv[2])

    flights = [None] * numFlights
    schedule = [None] * numFlights
    numUsedGates = 0

    for var in vars:
        flight = int(var[0])
        if flight < numFlights:
            flights[flight] = int(var[1])

    for i in range(numFlights):
        if schedule[i] is None:
            schedule[i] = getGate(i, flights, numFlights,
                                  schedule, numUsedGates)
        if schedule[i] >= numUsedGates:
            numUsedGates += 1

    print numUsedGates
    print " ".join(map(lambda x: str(x), schedule))

try:
    sys.stdout.close()
except:
    pass
try:
    sys.stderr.close()
except:
    pass
