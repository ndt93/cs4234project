#include <stdio.h>
#include <assert.h>
#include <vector>
#include <algorithm>

enum FlightState {
    ARRIVE, DEPART
};

struct Interval {
    int start;
    int end;
    int scheduledTime;
    int delay;
    FlightState state;
};

typedef std::vector<Interval> Gate; // A gate consists of a list of flights

Interval getInterval(int scheduledTime, int delay, FlightState state) {
    switch (state) {
        case ARRIVE:
            return {scheduledTime, scheduledTime + 45,
                    scheduledTime, delay, state};
        case DEPART:
            return {scheduledTime - 90, scheduledTime + 30,
                    scheduledTime, delay, state};
        default:
            assert(false);
    }
}

void scanFlightsData(FILE* input, FlightState flightState,
                     std::vector<Interval>& flights) {
    int numFlights;

    fscanf(input, "%d", &numFlights);

    for (int i = 0; i < numFlights; i++) {
        int scheduledTime = 0;
        float delay = 0;
        fscanf(input, "%d, %f", &scheduledTime, &delay);

        flights.push_back(getInterval(scheduledTime, (int)delay, flightState));
    }
}

std::vector<Gate> assignGates(std::vector<Interval> inputFlights) {
    std::vector<Interval> flights[2];
    flights[0] = inputFlights;

    std::sort(begin(flights[0]), end(flights[0]),
              [] (Interval a, Interval b) { return a.start < b.start; });

    std::vector<Gate> gates;
    int curFlightsSet = 0;

    while (!flights[curFlightsSet].empty()) {
        int nextFlightsSet = (curFlightsSet + 1) % 2;
        Gate gate;
        Interval *lastFlight = NULL;

        for (auto& flight : flights[curFlightsSet]) {
            if (lastFlight == NULL) {
                gate.push_back(flight);
                lastFlight = &flight;
            } else {
                if (flight.start < lastFlight->end) {
                    flights[nextFlightsSet].push_back(flight);
                } else {
                    gate.push_back(flight);
                    lastFlight = &flight;
                }
            }
        }

        flights[curFlightsSet].clear();
        curFlightsSet = nextFlightsSet;

        gates.push_back(gate);
    }

    return gates;
}

std::vector<Interval> accountForDelays(std::vector<Interval> flights) {
    std::vector<Interval> result;
    for (auto& flight : flights) {
        int actual_time = flight.scheduledTime + flight.delay;

        if (flight.state == DEPART) {
            result.push_back({actual_time - 90,
                              std::max(flight.end, actual_time + 10),
                              flight.scheduledTime, flight.delay, flight.state});
        } else if (flight.state == ARRIVE) {
            int actual_time = flight.scheduledTime + flight.delay;
            result.push_back({actual_time, actual_time + 45,
                              flight.scheduledTime, flight.delay, flight.state});
        }
    }

    return result;
}

int countNumConflicts(const Gate& gate) {
    std::vector<Interval> with_delays = accountForDelays(gate);
    int num_conflicts = 0;

    for (unsigned long i = 0; i < with_delays.size(); i++) {
        for (unsigned long j = i + 1; j < with_delays.size(); j++) {
            if (!(with_delays[1].start >= with_delays[0].end ||
                  with_delays[1].end <= with_delays[0].start)) {
                num_conflicts += 1;
            }
        }
    }

    return num_conflicts;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Usage: greedy_delays path_to_input\n");
        return 1;
    }
    FILE* input = fopen(argv[1], "r");
    if (input == NULL) {
        return 1;
    }

    std::vector<Interval> flights;
    int numFlights = 0;

    scanFlightsData(input, FlightState::ARRIVE, flights);
    scanFlightsData(input, FlightState::DEPART, flights);
    numFlights = flights.size();

    std::vector<Gate> gates = assignGates(flights);

    int num_conflicts = 0;
    for (auto& gate : gates) {
        num_conflicts += countNumConflicts(gate);
    }

    printf("%d, %ld, %d\n", numFlights, gates.size(), num_conflicts);

    fclose(input);

    return 0;
}
