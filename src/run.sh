#!/usr/bin/env bash

echo AIRPORT_ID,NUM_FLIGHTS,NUM_GATES > $1.out;

for d in ../Data/*/; do
    a=$(basename $d); # Airport ID
    echo -n $a, >> $1.out;
    ./greedy2 "${d}input/$1.in" >> $1.out;
done
