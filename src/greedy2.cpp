#include <stdio.h>
#include <assert.h>
#include <vector>
#include <algorithm>

struct Interval {
    int start;
    int end;
};

typedef std::vector<Interval> Gate; // A gate consists of a list of flights

enum FlightState {
    ARRIVE, DEPART
};

Interval getInterval(int scheduledTime, FlightState state) {
    switch (state) {
        case ARRIVE:
            return {scheduledTime, scheduledTime + 45};
        case DEPART:
            return {scheduledTime - 90, scheduledTime + 30};
        default:
            assert(false);
    }
}

void scanFlightsData(FILE* input, FlightState flightState,
                     std::vector<Interval>& flights) {
    int numFlights;

    fscanf(input, "%d", &numFlights);

    for (int i = 0; i < numFlights; i++) {
        int scheduledTime = 0;
        fscanf(input, "%d, %*f", &scheduledTime);

        flights.push_back(getInterval(scheduledTime, flightState));
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Usage: greedy path_to_input\n");
        return 1;
    }

    FILE* input = fopen(argv[1], "r");
    if (input == NULL) {
        return 1;
    }

    std::vector<Interval> flights[2];
    int numFlights = 0;

    scanFlightsData(input, FlightState::ARRIVE, flights[0]);
    scanFlightsData(input, FlightState::DEPART, flights[0]);
    numFlights = flights[0].size();

    std::sort(begin(flights[0]), end(flights[0]),
              [] (Interval a, Interval b) { return a.start < b.start; });

    std::vector<Gate> gates;
    int curFlightsSet = 0;

    while (!flights[curFlightsSet].empty()) {
        int nextFlightsSet = (curFlightsSet + 1) % 2;
        Gate gate;
        Interval *lastFlight = NULL;

        for (auto& flight : flights[curFlightsSet]) {
            if (lastFlight == NULL) {
                gate.push_back(flight);
                lastFlight = &flight;
            } else {
                if (flight.start < lastFlight->end) {
                    flights[nextFlightsSet].push_back(flight);
                } else {
                    gate.push_back(flight);
                    lastFlight = &flight;
                }
            }
        }

        flights[curFlightsSet].clear();
        curFlightsSet = nextFlightsSet;

        gates.push_back(gate);
    }

    printf("%d,%ld\n", numFlights, gates.size());

    fclose(input);

    return 0;
}
