#include <cstdlib>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <sstream>

#define MAX_LINE_LEN 550

enum FlightState {
    ARRIVE, DEPART, GATE
};

struct Interval {
    int start;
    int end;
    int scheduledTime;
    int delay;
    FlightState state;
};

typedef std::vector<Interval> Gate; // A gate consists of a list of flights

std::string intervalToString(const Interval& interval) {
    return "[" + std::to_string(interval.start) +
           ", " + std::to_string(interval.end) + ")";
}

void printGates(std::vector<Gate> gates) {
    for (auto& gate : gates) {
        std::sort(std::begin(gate), std::end(gate),
                  [] (Interval a, Interval b) { return a.start < b.start; });
        printf("G: ");
        for (auto& flight : gate) {
            printf("%s ", intervalToString(flight).c_str());
        }
        printf("\n");
    }

}

Interval getInterval(int scheduledTime, int delay, FlightState state) {
    switch (state) {
        case ARRIVE:
            return {scheduledTime, scheduledTime + 45,
                    scheduledTime, delay, state};
        case DEPART:
            return {scheduledTime - 90, scheduledTime + 30,
                    scheduledTime, delay, state};
        default:
            assert(false);
            return {};
    }
}

void scanFlightsData(FILE* input, FlightState flightState,
                     std::vector<Interval>& flights) {
    int numFlights;

    fscanf(input, "%d", &numFlights);

    for (int i = 0; i < numFlights; i++) {
        int scheduledTime = 0;
        float delay = 0;
        fscanf(input, "%d, %f", &scheduledTime, &delay);

        flights.push_back(getInterval(scheduledTime, (int)delay, flightState));
    }
}

std::vector<Gate> assignGates(std::vector<Interval> inputFlights) {
    std::vector<Interval> flights[2];
    flights[0] = inputFlights;

    std::sort(begin(flights[0]), end(flights[0]),
              [] (Interval a, Interval b) { return a.start < b.start; });

    std::vector<Gate> gates;
    int curFlightsSet = 0;

    while (!flights[curFlightsSet].empty()) {
        int nextFlightsSet = (curFlightsSet + 1) % 2;
        Gate gate;
        Interval *lastFlight = NULL;

        for (auto& flight : flights[curFlightsSet]) {
            if (lastFlight == NULL) {
                gate.push_back(flight);
                lastFlight = &flight;
            } else {
                if (flight.start < lastFlight->end) {
                    flights[nextFlightsSet].push_back(flight);
                } else {
                    gate.push_back(flight);
                    lastFlight = &flight;
                }
            }
        }

        flights[curFlightsSet].clear();
        curFlightsSet = nextFlightsSet;

        gates.push_back(gate);
    }

    return gates;
}

std::vector<Interval> accountForDelays(std::vector<Interval> flights) {
    std::vector<Interval> result;
    for (auto& flight : flights) {
        int actual_time = flight.scheduledTime + flight.delay;

        if (flight.state == DEPART) {
            result.push_back({actual_time - 90,
                              std::max(flight.end, actual_time + 10),
                              flight.scheduledTime, flight.delay, flight.state});
        } else if (flight.state == ARRIVE) {
            int actual_time = flight.scheduledTime + flight.delay;
            result.push_back({actual_time, actual_time + 45,
                              flight.scheduledTime, flight.delay, flight.state});
        }
    }

    return result;
}

int countNumConflicts(const Gate& gate) {
    std::vector<Interval> with_delays = accountForDelays(gate);
    int num_conflicts = 0;

    for (unsigned long i = 0; i < with_delays.size(); i++) {
        for (unsigned long j = i + 1; j < with_delays.size(); j++) {
            if (!(with_delays[1].start >= with_delays[0].end ||
                  with_delays[1].end <= with_delays[0].start)) {
                num_conflicts += 1;
            }
        }
    }

    return num_conflicts;
}

double reward(double duration) {
    return -1000 * (std::atan(0.21 * (5. - duration)) + M_PI / 2.) + 2381;
}

void resetStream(std::ostringstream& stream) {
    stream.clear();
    stream.str(std::string());
}

int generateLP(std::vector<Interval> flights, const std::vector<Gate>& gates,
               int numExtraGates, const char* outputFile) {
    int numFlights = flights.size();
    int numGates = gates.size() + numExtraGates;
    int sumFlightsGates = numFlights + numGates;
    assert(sumFlightsGates >= 1);

    int minStartTime = 9999999;
    int maxEndTime = -9999999;
    for (auto& flight : flights) {
        minStartTime = std::min(minStartTime, flight.start);
        maxEndTime = std::max(maxEndTime, flight.end);
    }
    for (int i = numFlights; i < sumFlightsGates; i++) {
        flights.push_back({maxEndTime, minStartTime, 0, 0, FlightState::GATE});
    }

    FILE* lpOutput = fopen(outputFile, "w");
    if (lpOutput == NULL) {
        return 1;
    }

    /* OBJECTIVE */
    fprintf(lpOutput, "Maximize\n");

    std::ostringstream stream;
    int curLineLen = 0;

    for (int i = 0; i < sumFlightsGates; i++) {
        for (int j = 0; j < sumFlightsGates; j++) {
            std::ostringstream tmp;

            if (i == 0 && j == 0) {
                tmp << reward(flights[j].start - flights[i].end)
                    << " x" << i << "_" << j;
            } else {
                tmp << " + " << reward(flights[j].start - flights[i].end)
                    << " x" << i << "_" << j;
            }

            int len = tmp.str().length();

            if (len + curLineLen > MAX_LINE_LEN) {
                stream << "\n";
                curLineLen = 0;
            }

            stream << tmp.str();
            curLineLen += len;
        }
    }

    fprintf(lpOutput, "%s\n", stream.str().c_str());

    /* CONSTRAINTS */
    fprintf(lpOutput, "Subject To\n");

    /* Each flight is followed by a flight */
    for (int i = 0; i < sumFlightsGates; i++) {
        stream.clear();
        stream.str(std::string());
        curLineLen = 0;

        stream << "x" << i << "_0";
        curLineLen += stream.str().length();

        for (int j = 1; j < sumFlightsGates; j++) {
            std::ostringstream tmp;
            tmp << " + x" << i << "_" << j;
            int len = tmp.str().length();

            if (len + curLineLen > MAX_LINE_LEN) {
                stream << "\n";
                curLineLen = 0;
            }

            stream << tmp.str();
            curLineLen += len;
        }

        if (curLineLen + 5 > MAX_LINE_LEN) {
            stream << "\n";
        }

        stream << " = 1";

        fprintf(lpOutput, "%s\n", stream.str().c_str());
    }

    /* Each flight is following a flight */
    for (int j = 0; j < sumFlightsGates; j++) {
        stream.clear();
        stream.str(std::string());
        curLineLen = 0;

        stream << "x" << "0_" << j;
        curLineLen += stream.str().length();

        for (int i = 1; i < sumFlightsGates; i++) {
            std::ostringstream tmp;
            tmp << " + x" << i << "_" << j;
            int len = tmp.str().length();

            if (len + curLineLen > MAX_LINE_LEN) {
                stream << "\n";
                curLineLen = 0;
            }

            stream << tmp.str();
            curLineLen += len;
        }

        if (curLineLen + 5 > MAX_LINE_LEN) {
            stream << "\n";
        }

        stream << " = 1";

        fprintf(lpOutput, "%s\n", stream.str().c_str());
    }

    /* All gaps must be positive */
    for (int i = 0; i < sumFlightsGates; i++) {
        stream.clear();
        stream.str(std::string());
        curLineLen = 0;

        stream << flights[0].start - flights[i].end << " x" << i << "_0";
        curLineLen += stream.str().length();

        for (int j = 1; j < sumFlightsGates; j++) {
            std::ostringstream tmp;
            tmp << " + " << flights[j].start - flights[i].end
                << " x" << i << "_" << j;
            int len = tmp.str().length();

            if (len + curLineLen > MAX_LINE_LEN) {
                stream << "\n";
                curLineLen = 0;
            }

            stream << tmp.str();
            curLineLen += len;
        }

        if (curLineLen + 5 > MAX_LINE_LEN) {
            stream << "\n";
        }

        stream << " >= 0";

        fprintf(lpOutput, "%s\n", stream.str().c_str());
    }

    /* INTEGER VARIABLES */
    fprintf(lpOutput, "%s\n", "Binary");

    stream.clear();
    stream.str(std::string());
    curLineLen = 0;

    for (int i = 0; i < sumFlightsGates; i++) {
        for (int j = 0; j < sumFlightsGates; j++) {
            std::ostringstream tmp;
            tmp << " x" << i << "_" << j;
            int len = tmp.str().length();

            if (len + curLineLen > MAX_LINE_LEN) {
                stream << "\n";
                curLineLen = 0;
            }

            stream << tmp.str();
            curLineLen += len;
        }
    }

    fprintf(lpOutput, "%s\n", stream.str().c_str());

    fprintf(lpOutput, "End\n");
    fclose(lpOutput);

    return 0;
}

int findSlackSchedule(const std::vector<Interval>& flights,
                      const std::vector<Gate>& gates,
                      const int numExtraGates) {
    /* Solve LP */
    int numFlights = flights.size();

    generateLP(flights, gates, numExtraGates, "tmp.lp");
    system("scip -b batch.scip >> /dev/null");

    /* Process LP solution */
    FILE* sol;
    char command[256];
    sprintf(command, "python parse_solution.py tmp.sol %d %ld",
            numFlights, gates.size() + numExtraGates);

    if (!(sol = (FILE*)popen(command, "r"))) {
        perror("popen");
        exit(1);
    }

    int numNewGates = 0;
    fscanf(sol, "%d", &numNewGates);
    std::vector<Gate> newGates(numNewGates);

    for (int i = 0; i < numFlights; i++) {
        int gate = -1;
        fscanf(sol, "%d", &gate);
        newGates[gate].push_back(flights[i]);
    }
    pclose(sol);

    //printGates(newGates);

    int numConflicts = 0;
    for (auto& gate : newGates) {
        numConflicts += countNumConflicts(gate);
    }

    return numConflicts;
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        printf("Usage: final path_to_input num_extra_gates\n");
        return 1;
    }
    FILE* input = fopen(argv[1], "r");
    if (input == NULL) {
        return 1;
    }

    std::vector<Interval> flights;
    int numFlights = 0;
    int numExtraGates = std::atoi(argv[2]);

    scanFlightsData(input, FlightState::ARRIVE, flights);
    scanFlightsData(input, FlightState::DEPART, flights);
    numFlights = flights.size();
    fclose(input);

    std::vector<Gate> gates = assignGates(flights);
    //printGates(gates);

    int numConflicts = 0;
    for (auto& gate : gates) {
        numConflicts += countNumConflicts(gate);
    }

    printf("%d, %ld, %d\n", numFlights, gates.size(), numConflicts);

    printf("%d, %d\n",
           numExtraGates,
           findSlackSchedule(flights, gates, numExtraGates));

    return 0;
}
