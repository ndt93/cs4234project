import sys
import os
from os.path import exists as path_exists
from os.path import join as join_path
from calendar import monthrange

import pandas as pd
import numpy as np

import util
from util import COLORS as COLORS

"""
Usage: generate_input <airport id> <month>
Result: for each day in <month> for the <airport_id>, a file <mont>-<day>.in
is generated in the <airport_id>/input directory with the content:
<num_of_arrival_flights>
<scheduled_time, delay>
<scheduled_time, delay>
...
<num of departure flights>
<scheduled_time, delay>
<scheduled_time, delay>
...
"""

def generate_arrival_input(arrive_file, result_path):
    df = pd.read_csv(arrive_file, dtype=util.columns_types)
    crs_arr_time = pd.to_datetime(df['CRS_ARR_TIME'].values, format='%H%M')
    delay = df['ARR_DELAY'].values

    with open(result_path, 'a') as f:
        f.write(str(crs_arr_time.shape[0]) + '\n')

        for (schedule, delay) in zip(crs_arr_time, delay):
            ontime = schedule.hour * 60 + schedule.minute
            delay = 0.0 if np.isnan(delay) else delay

            flight_str = '{}, {}\n'.format(ontime, delay)
            f.write(flight_str)

def generate_departure_input(depart_file, result_path):
    df = pd.read_csv(depart_file, dtype=util.columns_types)

    crs_dep_time = pd.to_datetime(df['CRS_DEP_TIME'].values, format='%H%M')
    delay = df['DEP_DELAY'].values

    with open(result_path, 'a') as f:
        f.write(str(crs_dep_time.shape[0]) + '\n')

        for (schedule, delay) in zip(crs_dep_time, delay):
            ontime = schedule.hour * 60 + schedule.minute
            delay = 0.0 if np.isnan(delay) else delay

            flight_str = '{}, {}\n'.format(ontime, delay)
            f.write(flight_str)

if __name__ == '__main__':
    airport_id = sys.argv[1]
    month = int(sys.argv[2])
    year = 2015
    airport_path = join_path('../Data', airport_id)

    result_path = join_path(airport_path, 'input')
    if not path_exists(result_path):
        os.makedirs(result_path)

    arrive_path = join_path(airport_path, 'arrive')
    depart_path = join_path(airport_path, 'depart')

    for day in range(1, monthrange(year, month)[1] + 1):
        filename = '{}-{}.csv'.format(month, day)
        arrive_file = join_path(arrive_path, filename)
        depart_file = join_path(depart_path, filename)
        generated_file = join_path(result_path, '{}-{}.in'.format(month, day))

        if path_exists(arrive_file) or path_exists(depart_file):
            util.touch(generated_file)
        else:
            continue

        if path_exists(arrive_file):
            generate_arrival_input(arrive_file, generated_file)

        if path_exists(depart_file):
            generate_departure_input(depart_file, generated_file)
