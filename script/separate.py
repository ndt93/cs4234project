import pandas as pd

import os
import sys

import util

"""
Usage: python separate.py <path_to_csv_file> [airport id]
"""

def separateByDates(df, save_path):
    dates = df['FL_DATE'].unique()
    pdDatetime = pd.DatetimeIndex(dates);
    days = pdDatetime.day
    months = pdDatetime.month

    for (month, day, date) in zip(months, days, dates):
        day_path = os.path.join(save_path, '{}-{}.csv'.format(month, day))
        df[df['FL_DATE'] == date].to_csv(day_path)


if __name__ == '__main__':
    base_path = os.path.dirname(sys.argv[1])

    df = pd.read_csv(sys.argv[1], parse_dates=['FL_DATE'],
                     dtype=util.columns_types)
    df = df.drop(df.columns[-1], axis=1)

    # Select the airport with max number of flights if no airport is provided
    if len(sys.argv) < 3:
        byOrigin = df[['ORIGIN_AIRPORT_ID', 'FL_DATE']]
        byOrigin = byOrigin.groupby(['ORIGIN_AIRPORT_ID']).agg(['count'])
        airport = byOrigin.idxmax().values[0]
    else:
        airport = int(sys.argv[2])

    depart = df[df['ORIGIN_AIRPORT_ID'] == airport]
    depart = depart[['FL_DATE', 'CRS_DEP_TIME', 'DEP_TIME', 'DEP_DELAY']]

    arrive = df[df['DEST_AIRPORT_ID'] == airport]
    arrive = arrive[['FL_DATE', 'CRS_ARR_TIME', 'ARR_TIME', 'ARR_DELAY']]

    arrive_path = os.path.join(base_path, str(airport) + '/arrive')
    depart_path = os.path.join(base_path, str(airport) + '/depart')

    if not os.path.exists(arrive_path):
        os.makedirs(arrive_path)
    if not os.path.exists(depart_path):
        os.makedirs(depart_path)

    separateByDates(arrive, arrive_path)
    separateByDates(depart, depart_path)
