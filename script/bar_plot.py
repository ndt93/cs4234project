import numpy as np
from matplotlib import pyplot as plt

N = 5
ind = np.arange(N)
width = 0.35

greedy = (111, 321, 239, 155, 100)
slack = (0, 28, 0, 21, 0)

fig, ax = plt.subplots()
rects1 = ax.bar(ind, greedy, width, color='r')
rects2 = ax.bar(ind + width, slack, width, color='b')

ax.set_ylabel('Number of conflicts')
ax.set_xlabel('Day of month')
ax.set_xticks(ind + width)
ax.set_xticklabels(('1st', '2nd', '3rd', '4th', '5th'))

ax.legend((rects1[0], rects2[0]), ('Greedy', 'Slack'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)

plt.show();
