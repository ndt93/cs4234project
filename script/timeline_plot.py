from matplotlib import pyplot as plt
import numpy as np

def timelines(y, xstart, xstop, color='b'):
    plt.hlines(y, xstart, xstop, color, lw=4)
    plt.vlines(xstart, y + 0.03, y - 0.03, color, lw=2)
    plt.vlines(xstop, y + 0.03, y - 0.03, color, lw=2)

cap = ('Gate 1', 'Gate 2', 'Gate 3')
captions, unique_idx, caption_inv = np.unique(cap, 1, 1)
y = (caption_inv + 1) / float(len(captions) + 1)

timelines(y[0], 750, 795)
timelines(y[0], 1228, 1348)

timelines(y[1], 518, 638)
timelines(y[1], 939, 1059)

timelines(y[2], 620, 665)
timelines(y[2], 950, 995)

plt.yticks(y[unique_idx], captions)
plt.ylim(0, 1)
plt.xlabel('Time (min)')
plt.show()
