import os
import numpy as np

class COLORS:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


columns_types = {'CRS_DEP_TIME': np.str, 'DEP_TIME': np.str,
                 'CRS_ARR_TIME': np.str, 'ARR_TIME': np.str}


def touch(path):
    with open(path, 'w'):
        os.utime(path, None)
