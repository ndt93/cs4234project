import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

if __name__ == '__main__':
    df = pd.read_csv(sys.argv[1])

    X = df['NUM_FLIGHTS']
    Y = df['NUM_GATES']

    fit = np.polyfit(X, Y, 1)
    fit_fn = np.poly1d(fit)

    plt.plot(X, Y, 'ro', X, fit_fn(X))
    plt.xlabel('Number of flights')
    plt.ylabel('Number of gates')
    plt.show()
